<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Patch;

use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Put;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use AppBundle\Entity\Route;

/**
 * Description of RouteControllerApi
 *
 * @author Ser
 */
class RouteApiController extends FOSRestController {

    /**
     * Get all the tasks
     * @return array
     *
     * @View()
     * @Get("/routes")
     */
    public function getRoutesAction() {

        $routes = $this->getDoctrine()->getRepository("AppBundle:Route")
                ->findAll();

        return array('routes' => $routes);
    }

    /**
     * Get a task by ID
     * @param Route $route
     * @return array
     *
     * @View()
     * @ParamConverter("route", class="AppBundle:Route")
     * @Get("/route/{id}",)
     */
    public function getRouteAction(Route $route) {

        return array('route' => $route);
    }

    /**
     * Create a new Route
     * @var Request $request
     * @return View|array
     *
     * @View()
     * @Post("/route")
     * 
     */
    public function postRouteAction(Request $request) {
        $route = new Route();
        $em = $this->getDoctrine()->getManager();
        $body = $request->getContent();
        $data = json_decode($body, TRUE);
        $form = $this->createForm('AppBundle\Form\RouteType', $route);
        $form->submit($data);
        if ($form->isValid()) {
            $route->setCreatedAt(new \DateTime());
            $em->persist($route);
            $em->flush();
            $view = $this->view($data, 201);
            return $view;
        }
        /* $route->setName($data['name']);
          $route->setDescription($data['description']);
          $route->setCreatedAt(new \DateTime());
          $route->setUpdatedAt(new \DateTime()); */
        return array("form" => $form);
    }

    /**
     * Edit a Route
     * Put action
     * @var Request $request
     * @var Route $route
     * @return array
     *
     * @View()
     * @ParamConverter("route", class="AppBundle:Route")
     * @Put("/route/{id}")
     * @Patch("/route/{id}")
     */
    public function putRouteAction(Request $request, Route $route) {
        
        $form = $this->createForm('AppBundle\Form\RouteType', $route);      
        $body = $request->getContent();
        $data = json_decode($body, TRUE);      
        $form->submit($data, false);
        if ($form->isValid()) {
            $route->setUpdatedAt(new \DateTime());            
            $em = $this->getDoctrine()->getManager();
            $em->persist($route);
            $em->flush();
            $view = $this->view($route, 200);
            return $view;
        }     
        return array("form" => $form);
    }
    
    /**
     * Delete a Route
     * Delete action
     * @var Route $route
     * @return array
     *
     * @View()
     * @ParamConverter("route", class="AppBundle:Route")
     * @Delete("/route/{id}")
     */
    public function deleteRouteAction(Route $route)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($route);
        $em->flush();

        return array("status" => "Deleted");
    }


}
